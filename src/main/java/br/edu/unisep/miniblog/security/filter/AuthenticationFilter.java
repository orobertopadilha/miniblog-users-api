package br.edu.unisep.miniblog.security.filter;

import br.edu.unisep.miniblog.domain.dto.AuthDto;
import br.edu.unisep.miniblog.domain.dto.LoginResultDto;
import br.edu.unisep.miniblog.domain.user.UserDto;
import br.edu.unisep.miniblog.security.data.UserAuthDetails;
import br.edu.unisep.miniblog.security.jwt.JwtProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static br.edu.unisep.miniblog.utils.Messages.MESSAGE_INVALID_LOGIN;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public AuthenticationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            AuthDto authData = new ObjectMapper().readValue(request.getInputStream(), AuthDto.class);

            return getAuthenticationManager().authenticate(
                    new UsernamePasswordAuthenticationToken(authData.getLogin(),
                            authData.getPassword(), new ArrayList<>())
            );
        } catch (IOException e) {
            throw new UsernameNotFoundException(MESSAGE_INVALID_LOGIN, e);
        }
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
                                            Authentication auth) throws IOException {

        var authUser = (UserAuthDetails) auth.getPrincipal();
        var userData = new UserDto(
                authUser.getUserId(),
                authUser.getUsername(),
                authUser.getName(),
                authUser.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())
        );
        var token = JwtProvider.createJwt(userData);

        var loginResult = new LoginResultDto(userData, token);

        var mapper = new ObjectMapper();
        res.setContentType("application/json");

        mapper.writeValue(res.getOutputStream(), loginResult);
    }
}
