package br.edu.unisep.miniblog.security.data;

import br.edu.unisep.miniblog.data.entity.User;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserAuthDetails implements UserDetails {

    private Integer userId;

    private Collection<GrantedAuthority> authorities;

    private String password;
    private String username;

    private String name;

    public static UserAuthDetails from(User user) {
        var details = new UserAuthDetails();
        details.userId = user.getId();
        details.username = user.getLogin();
        details.name = user.getName();
        details.password = user.getPassword();

        details.authorities = user.getRoles().stream().map(
                role -> new SimpleGrantedAuthority(role.getId())
        ).collect(Collectors.toList());

        return details;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
