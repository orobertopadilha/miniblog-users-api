package br.edu.unisep.miniblog.domain.dto;

import br.edu.unisep.miniblog.domain.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginResultDto {

    private final UserDto userData;

    private final String token;

}
