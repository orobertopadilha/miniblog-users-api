package br.edu.unisep.miniblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MiniblogUsersApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniblogUsersApiApplication.class, args);
	}

}
